import React from "react"

class AddToCard extends React.Component{
   constructor(){
       super();
       this.state={
           toggle:true
       }
   }

    onClick = () => {
        this.setState((prevState)=> ({
           toggle: !prevState.toggle
        })
        )
    };

    render (){
        return(
             <button className="ui secondary button">
                    Add to Card
            </button>
        );
    }
}

export default AddToCard;