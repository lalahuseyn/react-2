import React from 'react'
const Modal=(props)=>{
    return (
        <div className="ui modal mini" style={{ display: props.toggle ? "block" : "none" }}>
                <div className="header">Header</div>
                <div className="content">
                    <p>Content</p>
                </div>
           </div>
    )
}

export default Modal;