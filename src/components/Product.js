import React from 'react'
import AddToCard from './AddToCard';

class Product extends React.Component {
    constructor(props) {
        super(props);
        // this.stat
    }
    // callbackFunction = (value) => {
    //     this.setState({ message: childData })
    // }

    render() {
        const product = this.props.product;
        return (
            <div className="ui link cards">
                <div className="card">
                    <div className="image">
                        <img src={product.image} />
                    </div>
                    <div className="content">
                        <div className="header">{product.name}</div>
                        <div className="meta">
                            <a>Friends</a>
                        </div>
                        <div className="description">
                            Matthew is an interior designer living in New York.
                        </div>
                    </div>
                    <div className="extra content">
                        <span className="right floated">
                            <AddToCard />
                        </span>
                        <span>
                            {product.price} $
                        </span>
                    </div>
                </div>
            </div>
        )
    }
}

export default Product;