import React from 'react'
import Product from './Product';
import Axios from 'axios';
import Modal from './ModalCard';

class ProductList extends React.Component {
    constructor() {
        super();
        this.state = {
            products: []
        }
    }
    
    componentDidMount() {
        Axios.get("products.json")
            .then(response => {
                this.setState({
                    products: response.data.products
                })
            })
    }
    render() {
        return (
            <div className="ui three column grid">
                 {this.state.products.map(product =><Product key={product.id} product={product} />)}
                <Modal toggle={true}/>
            </div>
        )
    }
}

export default ProductList;